Higgason Construction is a licensed & bonded general contractor located in Sammamish, Washington. We provide precision results and help with unique designs. We work on fine and luxury home upgrades, expansions, and repairs. Call (425) 577-8512 for more information.

Address: 24846 SE 19th St, Sammamish, WA 98075

Phone: 425-577-8512
